const mariadb = require('mariadb');
const pool = mariadb.createPool({
     host: 'localhost', 
     port: '3307',
     user:'root', 
     password: 'Maria1234000',
     database: 'nodejs_prueba'
});

async function asyncFunction() {
    let conn;
    try {
      conn = await pool.getConnection();
      const rows = await conn.query("SELECT 1 as val");
      console.log(rows); //[ {val: 1}, meta: ... ]
      const res = await conn.query("INSERT INTO myTable value (?, ?)", [1, "mariadb"]);
      console.log(res); // { affectedRows: 1, insertId: 1, warningStatus: 0 }
  
    } catch (err) {
      throw err;
    } finally {
      if (conn) return conn.end();
    }
  }

pool.getConnection()
    .then(conn => {
    
      conn.query("SELECT 1 as val")
        .then((rows) => {
          console.log(rows); //[ {val: 1}, meta: ... ]
          //Table must have been created before 
        //   " CREATE TABLE myTable (id int, val varchar(255)) "
        //   return conn.query("INSERT INTO myTable value (?, ?)", [2, "mariadb2"]);
        return conn.query("INSERT INTO myTable values (?, ?)", [2, "mariadb2"]);
        })
        .then((res) => {
          console.log(res); // { affectedRows: 1, insertId: 1, warningStatus: 0 }
          conn.end();
        })
        .catch(err => {
          //handle error
          console.log(err); 
          conn.end();
        })
        
    }).catch(err => {
      //not connected
    });


//Bibliografia
// https://mariadb.com/kb/en/connector-nodejs-promise-api/
// https://hevodata.com/learn/nodejs-mariadb-integration/